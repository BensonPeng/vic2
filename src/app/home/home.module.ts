import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { AsideComponent } from './aside/aside.component';
import { HeaderComponent } from './header/header.component';
import { CustomMaterialModule } from '../custom-material.module';
import { EmployeeModule } from '../employee/employee.module';


@NgModule({
  declarations: [
    HomeComponent,
    AsideComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    CustomMaterialModule,
    EmployeeModule
  ]
})
export class HomeModule { }
