import { Routes } from '@angular/router';
import { AddressBookComponent } from './address-book/address-book.component';
import { CalendarComponent } from './calendar/calendar.component';
import { EimburseComponent } from './eimburse/eimburse.component';
import { FileComponent } from './file/file.component';
import { LeaveComponent } from './leave/leave.component';
import { LogbookComponent } from './logbook/logbook.component';
import { ToDoListComponent } from './to-do-list/to-do-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'to-do-list', pathMatch: 'full' },
  { path: 'address-book', component: AddressBookComponent },
  { path: 'calendar', component: CalendarComponent },
  { path: 'file', component: FileComponent },
  { path: 'leave', component: LeaveComponent },
  { path: 'logbook', component: LogbookComponent },
  { path: 'reimburse', component: EimburseComponent },
  { path: 'to-do-list', component: ToDoListComponent }];

export const employeeRouter = routes;

