import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarComponent } from './calendar/calendar.component';
import { AddressBookComponent } from './address-book/address-book.component';
import { LogbookComponent } from './logbook/logbook.component';
import { ToDoListComponent } from './to-do-list/to-do-list.component';
import { FileComponent } from './file/file.component';
import { LeaveComponent } from './leave/leave.component';
import { EimburseComponent } from './eimburse/eimburse.component';
// import { EmployeeRoutingModule } from './employee-routing.module';



@NgModule({
  declarations: [
    CalendarComponent,
    AddressBookComponent,
    LogbookComponent,
    ToDoListComponent,
    FileComponent,
    LeaveComponent,
    EimburseComponent
  ],
  imports: [
    CommonModule
    // ,
    // EmployeeRoutingModule
  ]
})
export class EmployeeModule { }
