import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EimburseComponent } from './eimburse.component';

describe('EimburseComponent', () => {
  let component: EimburseComponent;
  let fixture: ComponentFixture<EimburseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EimburseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EimburseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
